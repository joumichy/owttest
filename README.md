# Owttest


Auteur : John ALLOU

## Information

- API Restfull spring boot permettant l'utisation des methodes CRUD sur une liste de bateau.

- Dans le cadre de ce test l'utilisation de fichier .ENV n'a pas été prise en compte.

- Le projet respecte toutes les contraintes imposées dans les spec.

- l'API est utilisable avec sa partie front: https://gitlab.com/joumichy/owttestfront

- L'architecture de ce projet est sous le model clean-architecture.

## Routes : 
Documentation disponible sur le swagger.

Lien :  http://localhost:8080/swagger-ui/


