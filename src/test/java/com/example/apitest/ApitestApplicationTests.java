package com.example.apitest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootTest
@EnableSwagger2
class ApitestApplicationTests {

    @Test
    void contextLoads() {
    }

}
