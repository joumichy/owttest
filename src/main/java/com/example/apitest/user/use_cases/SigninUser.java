package com.example.apitest.user.use_cases;

import com.example.apitest.security.jwt.JwtResponse;
import com.example.apitest.security.jwt.JwtFactory;
import com.example.apitest.security.user.UserDetailsImpl;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


@Service
public class SigninUser {

    private final AuthenticationManager authenticationManager;

    private final JwtFactory jwtFactory;

    public SigninUser(AuthenticationManager authenticationManager, JwtFactory jwtFactory) {
        this.authenticationManager = authenticationManager;
        this.jwtFactory = jwtFactory;
    }

    public JwtResponse execute(String username, String password){

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtFactory.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername()
                );
    }

}
