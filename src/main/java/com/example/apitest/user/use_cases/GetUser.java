package com.example.apitest.user.use_cases;

import com.example.apitest.user.infrastructure.controller.dto.UserEntityDto;
import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.user.domain.model.UserModel;
import com.example.apitest.user.domain.model.IUserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GetUser {


    private final IUserRepository userRepository;

    public GetUser(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<UserEntityDto> execute(String username, String password) throws UserException {
        UserModel userModel = new UserModel(username, password);
        verificationOf(userModel);
        return userRepository.findUserByUsername(userModel.getUsername());
    }

    public void verificationOf(UserModel userModel) throws UserException{
        if(userModel.getPassword().isEmpty() || userModel.getPassword().isBlank()) throw new UserException("Nom d'utilisateur invalide");

    }
}
