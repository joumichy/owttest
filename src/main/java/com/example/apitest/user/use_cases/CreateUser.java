package com.example.apitest.user.use_cases;

import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.user.domain.model.UserModel;
import com.example.apitest.user.domain.model.IUserRepository;
import org.springframework.stereotype.Service;

@Service
public class CreateUser {


    private final IUserRepository userRepository;

    public CreateUser(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void execute(String username, String password) throws UserException {
        UserModel userModel = new UserModel( username, password);
        verificationOf(userModel);
        userRepository.createUser(userModel);

    }

    public void verificationOf(UserModel userModel) throws UserException{
        if(userModel.getUsername().length() < 4) throw new UserException("Nom d'utilisateur infériur à 4 caractères");
        if(userModel.getPassword().isEmpty() || userModel.getPassword().isBlank()) throw new UserException("Mot de passe invalide");
    }
}
