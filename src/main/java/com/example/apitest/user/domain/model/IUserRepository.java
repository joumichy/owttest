package com.example.apitest.user.domain.model;

import com.example.apitest.user.infrastructure.dao.UserEntity;
import com.example.apitest.user.infrastructure.controller.dto.UserEntityDto;
import com.example.apitest.user.infrastructure.controller.dto.UserEntityDtoC;

import java.util.Optional;

public interface IUserRepository {
    void createUser(UserModel user) throws UserException;
    Boolean existsByUsername(String username);

    Optional<UserEntity> findByUsername(String username);
    Optional<UserEntityDto> findUserByUsername(String username) throws UserException;
    Optional<UserEntityDtoC> findUserByUsernameC(String username) throws UserException;


    Optional<UserModel> findUserById(Integer id) throws UserException;
}
