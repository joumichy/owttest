package com.example.apitest.user.domain.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel {

    @JsonProperty("id")
    private Integer id;
    private  String username;
    private  String password;


    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }


    public UserModel(Integer id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
    public UserModel(){

    }
    public UserModel(Integer id) {
        this.id = id;
    }

    public UserModel(String username) {
        this.username = username;
    }

    public UserModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }



}
