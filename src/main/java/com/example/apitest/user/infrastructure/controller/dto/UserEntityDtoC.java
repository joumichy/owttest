package com.example.apitest.user.infrastructure.controller.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserEntityDtoC implements Serializable {
    private final Integer id;
    private final String username;
    private final String password;
}
