package com.example.apitest.user.infrastructure.dao;

import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.user.domain.model.UserModel;
import com.example.apitest.user.infrastructure.controller.dto.UserEntityDto;
import com.example.apitest.user.infrastructure.controller.dto.UserEntityDtoC;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserAdapter {



    private static PasswordEncoder encoder;

    public UserAdapter(PasswordEncoder encoder){
        UserAdapter.encoder = encoder;
    }



    public static UserEntity convertToUserEntity(UserModel userModel) {
        return new UserEntity(
                userModel.getUsername(),
                encoder.encode(userModel.getPassword())
                );
    }


    public static UserEntity convertToUserEntityWithId(UserModel userModel) {
        return new UserEntity(
                userModel.getId(),
                userModel.getUsername(),
                userModel.getPassword()
        );
    }

    public static UserModel convertToUser(UserEntity userEntity) {
        return new UserModel(
                userEntity.getId(),
                userEntity.getUsername(),
                userEntity.getPassword()
        );
    }

    public static UserEntityDto convertToUserDTO(UserEntity userEntity) {

        return new UserEntityDto(userEntity.getId(),userEntity.getUsername());

    }

    public static UserEntityDtoC convertToUserDTOC(UserEntity userEntity) {

        return new UserEntityDtoC(userEntity.getId(),userEntity.getUsername(), userEntity.getPassword());

    }




}
