package com.example.apitest.user.infrastructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserInputDTO {
     private String username;
     private String password;




}
