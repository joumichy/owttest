package com.example.apitest.user.infrastructure.controller;

import com.example.apitest.user.infrastructure.controller.dto.UserInputDTO;
import com.example.apitest.security.jwt.JwtResponse;
import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.user.use_cases.CreateUser;
import com.example.apitest.user.use_cases.GetUser;
import com.example.apitest.user.use_cases.SigninUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    CreateUser createUser;
    @Autowired
    GetUser getUser;

    @Autowired
    SigninUser signinUser;


    @PostMapping("/signup")
    public ResponseEntity<?> signUpUser(@RequestBody UserInputDTO userDTO) {

        try {
            createUser.execute(
                    userDTO.getUsername(),
                    userDTO.getPassword()
             );

        } catch (UserException | RuntimeException e) {
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                "User registered successfully !",
                HttpStatus.CREATED);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> signInUser(@RequestBody UserInputDTO userDTO) {

        try {
            JwtResponse userModel = signinUser.execute(
                    userDTO.getUsername(),
                    userDTO.getPassword()
            );
            return new ResponseEntity<>(userModel, HttpStatus.OK);


        } catch (RuntimeException e) {
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }

    }


}
