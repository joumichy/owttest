package com.example.apitest.user.infrastructure.controller.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserEntityDto implements Serializable {
    private final Integer id;
    private final String username;

}


