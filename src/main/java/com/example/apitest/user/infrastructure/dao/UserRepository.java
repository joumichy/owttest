package com.example.apitest.user.infrastructure.dao;

import com.example.apitest.user.domain.model.IUserRepository;
import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.user.domain.model.UserModel;
import com.example.apitest.user.infrastructure.controller.dto.UserEntityDto;
import com.example.apitest.user.infrastructure.controller.dto.UserEntityDtoC;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer>, IUserRepository {

    default Optional<UserEntityDto> findUserByUsername(@Param("username") @NonNull String username) throws UserException {
        Optional<UserEntity> userEntity = findByUsername(username);

        if(userEntity.isEmpty()) throw new UserException("Utilisateur introuvable");
        return Optional.of(UserAdapter.convertToUserDTO(userEntity.get()));
    }

    default Optional<UserEntityDtoC> findUserByUsernameC(@Param("username") @NonNull String username) throws UserException {
        Optional<UserEntity> userEntity = findByUsername(username);

        if(userEntity.isEmpty()) throw new UserException("Utilisateur introuvable");
        return Optional.of(UserAdapter.convertToUserDTOC(userEntity.get()));
    }

    default Optional<UserModel> findUserById(@Param("id") Integer id) throws UserException {
        Optional<UserEntity> userEntity = findById(id);
        if(userEntity.isEmpty()) throw new UserException("Utilisateur introuvable");
        return Optional.of(UserAdapter.convertToUser(userEntity.get()));

    }


    default void createUser(UserModel user) throws UserException {
        if(existsByUsername(user.getUsername())) throw new UserException("User already exist");

        UserEntity userEntitySaved = save(UserAdapter.convertToUserEntity(user));
        UserAdapter.convertToUserDTO(userEntitySaved);

    };
}