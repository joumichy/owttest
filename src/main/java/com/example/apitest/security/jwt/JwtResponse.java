package com.example.apitest.security.jwt;

public class JwtResponse {


    private String token;
    private Integer id;
    private String username;


    public JwtResponse(String accessToken, Integer id, String username) {
        this.token = accessToken;
        this.id = id;
        this.username = username;

    }

    public String getToken() {
        return token;
    }

    public void setToken(String accessToken) {
        this.token = accessToken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
