package com.example.apitest.security.user;


import com.example.apitest.user.domain.model.IUserRepository;
import com.example.apitest.user.infrastructure.controller.dto.UserEntityDtoC;
import com.example.apitest.user.domain.model.UserException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    IUserRepository userRepository;

    public UserDetailsServiceImpl(IUserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        Optional<UserEntityDtoC> user = Optional.empty();
        try {
            user = userRepository.findUserByUsernameC(username);
        } catch (UserException e) {
            e.printStackTrace();
        }
        assert user.isPresent();
        return UserDetailsImpl.build(user.get());
    }}
