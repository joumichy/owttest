package com.example.apitest.boat.use_cases;

import com.example.apitest.boat.domain.model.BoatException;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatEntityDto;
import com.example.apitest.boat.domain.model.IBoatRepository;
import com.example.apitest.security.user.UserDetailsImpl;
import com.example.apitest.user.domain.model.UserException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GetUserBoats {

    private final IBoatRepository boatRepository;

    public GetUserBoats(IBoatRepository boatRepository) {
        this.boatRepository = boatRepository;
    }

    public Optional<List<BoatEntityDto>> execute() throws BoatException, UserException {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return boatRepository.findAllByUserId(userDetails.getId());
    }

}
