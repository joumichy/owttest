package com.example.apitest.boat.use_cases;

import com.example.apitest.boat.domain.model.BoatException;
import com.example.apitest.boat.domain.model.BoatModel;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatEntityDto;
import com.example.apitest.boat.domain.model.IBoatRepository;
import com.example.apitest.user.domain.model.IUserRepository;
import com.example.apitest.security.user.UserDetailsImpl;
import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.user.domain.model.UserModel;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class EditBoat {

    private final IBoatRepository boatRepository;
    private final IUserRepository userRepository;

    public EditBoat(IBoatRepository boatRepository, IUserRepository userRepository) {
        this.boatRepository = boatRepository;
        this.userRepository = userRepository;
    }


    public BoatEntityDto execute(Integer id, String name, Integer size, String description) throws BoatException, UserException {

        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();

        Optional<UserModel> userModel = userRepository.findUserById(userDetails.getId());
        Optional<BoatEntityDto> editBoat = boatRepository.findBoatById(id);
        BoatModel boatModel = new BoatModel(editBoat.get().getId(),userModel.get(), name, size, description);
        verificationOf(name, size, description, editBoat.get(), userModel.get());
        return boatRepository.editBoat(boatModel);

    }

    public void verificationOf(String name, int size, String description, BoatEntityDto boatEntityDto, UserModel userModel) throws BoatException {
        if(name.isEmpty()) throw  new BoatException("Inserrer un nom de bateau");
        if(name.length() > 50) throw  new BoatException("Nom de bateau trop suppérieur à 50 caratères");
        if(description.length() > 300 ) throw  new BoatException("Description supérieur à 300 caractrères");

        if(size < 1 ) throw  new BoatException("Inserrer une taille supérieur à 1");

        if(boatEntityDto.getUser().getId() != userModel.getId()) throw  new BoatException("Vous n'êtes pas le propriétaire du bateau");

    }
}
