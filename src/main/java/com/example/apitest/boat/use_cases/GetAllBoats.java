package com.example.apitest.boat.use_cases;

import com.example.apitest.boat.domain.model.BoatException;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatEntityDto;
import com.example.apitest.boat.domain.model.IBoatRepository;
import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.user.domain.model.UserModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GetAllBoats {

    private final IBoatRepository boatRepository;

    public GetAllBoats(IBoatRepository boatRepository) {
        this.boatRepository = boatRepository;
    }


    public Optional<List<BoatEntityDto>> execute() throws BoatException, UserException {

        return boatRepository.findAllBoats();
    }

    public void verificationOf(UserModel userModel) throws UserException{
        if(userModel.getPassword().isEmpty() || userModel.getPassword().isBlank()) throw new UserException("Nom d'utilisateur invalide");

    }
}
