package com.example.apitest.boat.use_cases;

import com.example.apitest.boat.domain.model.BoatException;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatEntityDto;
import com.example.apitest.boat.domain.model.IBoatRepository;
import com.example.apitest.user.domain.model.IUserRepository;
import com.example.apitest.security.user.UserDetailsImpl;
import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.user.domain.model.UserModel;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class DeleteBoat {

    private final IBoatRepository boatRepository;
    private final IUserRepository userRepository;

    public DeleteBoat(IBoatRepository boatRepository, IUserRepository userRepository) {
        this.boatRepository = boatRepository;
        this.userRepository = userRepository;
    }


    public void execute(Integer id) throws BoatException, UserException {

        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Optional<UserModel> userModel = userRepository.findUserById(userDetails.getId());
        Optional<BoatEntityDto> boatEntityDto = boatRepository.findBoatById(id);
        verificationOf(userModel, boatEntityDto);

        boatRepository.deleteBoat(boatEntityDto.get().getId());

    }

    public void verificationOf(Optional<UserModel> userModel, Optional<BoatEntityDto> boatEntityDto) throws UserException, BoatException{
        //TODO : check that user exist
        if(userModel.isEmpty()) throw new UserException("Utilisateur introuvable");
        if(boatEntityDto.isEmpty()) throw new BoatException("Boat not found");
        if(boatEntityDto.get().getUser().getId() != userModel.get().getId()) throw new BoatException("You're not the boat owner ");

    }
}
