package com.example.apitest.boat.domain.model;

import com.example.apitest.user.domain.model.UserModel;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class BoatModel {

    private Integer id;
    private final UserModel user;
    private final String name;
    private final Integer size;
    private final String description;

    public BoatModel( UserModel user, String name, Integer size, String description) {

        this.user = user;
        this.name = name;
        this.size = size;
        this.description = description;
    }



}
