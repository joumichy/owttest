package com.example.apitest.boat.domain.model;

import com.example.apitest.boat.infrasctructure.controller.dto.BoatEntityDto;
import com.example.apitest.boat.infrasctructure.dao.BoatEntity;
import com.example.apitest.user.domain.model.UserException;

import java.util.List;
import java.util.Optional;

public interface IBoatRepository {
    boolean existsByUser_IdAndName(Integer id, String name);

    BoatEntityDto saveBoat(BoatModel boatModel) throws BoatException;

    BoatEntityDto editBoat(BoatModel boatModel) throws  BoatException;

    void deleteBoat(Integer id) throws BoatException;

    Optional<List<BoatEntityDto>> findAllByUserId(Integer userId) throws UserException, BoatException;
    Optional<List<BoatEntityDto>> findAllBoats() throws UserException, BoatException;

    List<BoatEntity> findByUser_Id(Integer id);


    Optional<BoatEntityDto> findBoatById(Integer id) throws BoatException;

}
