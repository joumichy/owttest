package com.example.apitest.boat.domain.model;

public class BoatException extends Exception{
    public BoatException(String message) {
        super(message);
    }
}
