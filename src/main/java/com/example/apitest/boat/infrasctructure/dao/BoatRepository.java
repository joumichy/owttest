package com.example.apitest.boat.infrasctructure.dao;

import com.example.apitest.boat.domain.model.BoatException;
import com.example.apitest.boat.domain.model.BoatModel;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatEntityDto;
import com.example.apitest.boat.domain.model.IBoatRepository;
import com.example.apitest.user.domain.model.UserException;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface BoatRepository extends JpaRepository<BoatEntity, Integer>, IBoatRepository {



    default BoatEntityDto saveBoat(BoatModel boatModel) throws BoatException {

        if(existsByUser_IdAndName(boatModel.getUser().getId(), boatModel.getName())) throw new BoatException("A Boat with this name already exist");

        BoatEntity boatEntity = save(BoatAdapter.convertToBoatEntity(boatModel));
        return BoatAdapter.convertToBoatDTO(boatEntity);

    }


    @Override
    default Optional<BoatEntityDto> findBoatById(Integer id) throws  BoatException{
        Optional<BoatEntity> boatEntity = findById(id);
        if(boatEntity.isEmpty()) throw new BoatException("Boat not found");
        return Optional.of(BoatAdapter.convertToBoatDTO(boatEntity.get()));
    }

    @Override
    default void deleteBoat(Integer id){
        deleteById(id);
    }

    @Override
    default BoatEntityDto editBoat(BoatModel boatModel) throws BoatException {

        BoatEntity boatEntity = save(BoatAdapter.convertToBoatEntityWithId(boatModel));
        System.out.println(boatEntity);
        return null;
    }


    @Override
    default Optional<List<BoatEntityDto>> findAllByUserId(Integer id) {
        List<BoatEntity> boats = findByUser_Id(id);
        List<BoatEntityDto> boatsDto = new ArrayList<>();
        for(BoatEntity boat : boats ) {
            boatsDto.add(BoatAdapter.convertToBoatDTO(boat));
        }
        return Optional.of(boatsDto);

    }

    @Override
    default Optional<List<BoatEntityDto>> findAllBoats() throws UserException, BoatException {
        List<BoatEntity> boats = findAll();
        List<BoatEntityDto> boatsDto = new ArrayList<>();
        for(BoatEntity boat : boats ) {
            boatsDto.add(BoatAdapter.convertToBoatDTO(boat));
        }
        return Optional.of(boatsDto);

    }






}