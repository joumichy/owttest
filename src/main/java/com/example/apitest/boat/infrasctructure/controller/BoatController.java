package com.example.apitest.boat.infrasctructure.controller;

import com.example.apitest.boat.domain.model.BoatException;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatEntityDto;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatInputCreateDTO;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatInputEditDTO;
import com.example.apitest.boat.infrasctructure.controller.dto.BoatResponse;
import com.example.apitest.boat.use_cases.CreateBoat;
import com.example.apitest.boat.use_cases.DeleteBoat;
import com.example.apitest.boat.use_cases.EditBoat;
import com.example.apitest.boat.use_cases.GetAllBoats;
import com.example.apitest.user.domain.model.UserException;
import com.example.apitest.boat.use_cases.GetUserBoats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/boat")
public class BoatController {

    @Autowired
    GetUserBoats getUserBoats;

    @Autowired
    GetAllBoats getAllBoats;

    @Autowired
    CreateBoat createBoat;

    @Autowired
    EditBoat editBoat;

    @Autowired
    DeleteBoat deleteBoat;

    @GetMapping("")
    public ResponseEntity<?> getBoatsFromUser() {

        try {
            Optional<List<BoatEntityDto>> boats = getUserBoats.execute();
            return new ResponseEntity<>(
                    boats,
                    HttpStatus.OK);

        } catch (BoatException | UserException | RuntimeException e) {
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllBoats() {

        try {
            Optional<List<BoatEntityDto>> boats = getAllBoats.execute();
            return new ResponseEntity<>(
                    boats,
                    HttpStatus.OK);

        } catch (BoatException| UserException | RuntimeException e) {
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/create")
    public ResponseEntity<?> createBoat(@RequestBody BoatInputCreateDTO boatInputDTO) {

        try {


            createBoat.execute(
                    boatInputDTO.getName(),
                    boatInputDTO.getSize(),
                    boatInputDTO.getDescription()
            );
            return new ResponseEntity<>(new BoatResponse("Boat registred successfuly",0), HttpStatus.CREATED);


        } catch (BoatException | UserException | RuntimeException e) {
            System.out.println(e);
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }


    }


    @PutMapping("/edit")
    public ResponseEntity<?> editBoat(@RequestBody BoatInputEditDTO boatInputDTO) {

        try {

            editBoat.execute(
                    boatInputDTO.getId(),
                    boatInputDTO.getName(),
                    boatInputDTO.getSize(),
                    boatInputDTO.getDescription()
            );
            return new ResponseEntity<>(new BoatResponse("Boat edited successfully", 0), HttpStatus.OK);


        } catch (BoatException | UserException | RuntimeException e) {
            System.out.println(e);
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }


    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteBoat(@RequestParam Integer boatId) {

        try {

            deleteBoat.execute(
                    boatId
            );
            return new ResponseEntity<>(new BoatResponse("Boat deleted successfuly",0), HttpStatus.OK);


        } catch (BoatException | UserException | RuntimeException e) {
            System.out.println(e);
            return new ResponseEntity<>(
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }


    }
}
