package com.example.apitest.boat.infrasctructure.dao;

import com.example.apitest.boat.infrasctructure.controller.dto.BoatEntityDto;
import com.example.apitest.boat.domain.model.BoatModel;
import com.example.apitest.user.infrastructure.dao.UserAdapter;
import org.springframework.stereotype.Component;

@Component
public class BoatAdapter {


    public static BoatEntity convertToBoatEntity(BoatModel model) {
        return new BoatEntity(
                UserAdapter.convertToUserEntityWithId(model.getUser()),
                model.getName(),
                model.getSize(),
                model.getDescription()
        );
    }

    public static BoatEntity convertToBoatEntityWithId(BoatModel model) {
        return new BoatEntity(
                model.getId(),
                UserAdapter.convertToUserEntityWithId(model.getUser()),
                model.getName(),
                model.getSize(),
                model.getDescription()
        );
    }



    public static BoatEntityDto convertToBoatDTO(BoatEntity boatEntity) {

        return new BoatEntityDto(boatEntity.getId(),
                UserAdapter.convertToUserDTO(boatEntity.getUser()),
                boatEntity.getName(),boatEntity.getSize(),
                boatEntity.getDescription());

    }

}
