package com.example.apitest.boat.infrasctructure.controller.dto;

import com.example.apitest.user.domain.model.UserModel;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BoatInputEditDTO {

    private  Integer id;
    private  String name;
    private  Integer size;
    private  String description;

}

