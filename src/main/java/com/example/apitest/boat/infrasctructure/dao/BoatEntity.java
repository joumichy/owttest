package com.example.apitest.boat.infrasctructure.dao;

import com.example.apitest.user.infrastructure.dao.UserEntity;

import javax.persistence.*;

@Entity
@Table(name = "boats")
public class BoatEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "name", length = 20)
    private String name;

    @Column(name = "size", nullable = false)
    private Integer size;

    @Column(name = "description", length = 200)
    private String description;

    public BoatEntity() {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BoatEntity(UserEntity user, String name, Integer size, String description) {
        this.user = user;
        this.name = name;
        this.size = size;
        this.description = description;
    }

    public BoatEntity(Integer id, UserEntity user, String name, Integer size, String description) {
        this.id = id;
        this.user = user;
        this.name = name;
        this.size = size;
        this.description = description;
    }
}