package com.example.apitest.boat.infrasctructure.controller.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class BoatInputCreateDTO {

    private String name;
    private Integer size;
    private  String description;



}

