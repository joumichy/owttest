package com.example.apitest.boat.infrasctructure.controller.dto;

import com.example.apitest.user.infrastructure.controller.dto.UserEntityDto;
import lombok.Data;

import java.io.Serializable;

@Data
public class BoatEntityDto implements Serializable {
    private final Integer id;
    private final UserEntityDto user;
    private final String name;
    private final Integer size;
    private final String description;
}
