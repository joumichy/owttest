package com.example.apitest.boat.infrasctructure.controller.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class BoatResponse   {
    String message;
    Integer code;
}
